package co.com.ajac.base.events;

public interface Data <T> {
    T get();
}
